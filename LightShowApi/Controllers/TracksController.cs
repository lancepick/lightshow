﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using LightShow.DataAccess;
using LightShow.Models;

namespace LightShowApi.Controllers
{
#if DEBUG
    [RoutePrefix("api")]
#endif
    public class TracksController : ApiController
    {
        // GET: api/Tracks
        [HttpGet]
        [Route("Tracks")]
        public IEnumerable<Track> Get()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetTracks();
            }
        }

        // GET: api/Tracks/5
        [HttpGet]
        [Route("Tracks/{id:int}")]
        public Track Get(int id)
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetTrack(id);
            }
        }

        [HttpGet]
        [Route("NowPlaying")]
        public PlaylistTrack GetNowPlayingTrack()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetNowPlayingTrack();
            }
        }

        [HttpGet]
        [Route("Queue")]
        public IEnumerable<Track> GetPlaylistQueue()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetPlaylistQueue();
            }
        }

        [HttpGet]
        [Route("LoadTrack")]
        [EnableCors(origins: "*", headers: "*", methods: "POST")]
        public string PostLoadNextTrack()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetPlaylistNextTrack();
            }
        }

        [HttpGet]
        [Route("Playlist/Add/{trackId:int}")]
        public bool AddTrack(int trackId)
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.AddTrackToPlaylist(trackId);
            }
        }

        [HttpGet]
        [Route("Playlist/History")]
        public IEnumerable<PlaylistTrack> GetPlaylistHistory()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetPlaylistHistory();
            }
        }

        [HttpGet]
        [Route("KillSwitch")]
        [EnableCors(origins: "*", headers: "*", methods: "GET")]
        public bool GetKillSwitch()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetKillSwitch();
            }
        }

        [HttpGet]
        [Route("KillSwitch/{on:int}")]
        public void SetKillSwitch(int on)
        {
            using (var ctx = new LightShowContext())
            {
                ctx.SetKillSwitch(on==1);
            }
        }

        [HttpGet]
        [Route("LoadRandomSong")]
        [EnableCors(origins: "*", headers: "*", methods: "GET")]
        public string GetRandomSong()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetRandomSong();
            }
        }

        [HttpGet]
        [Route("ClearQueue")]
        public void ClearQueue()
        {
            using (var ctx = new LightShowContext())
            {
                ctx.ClearQueue();
            }
        }

        [HttpGet]
        [Route("LibrarySync")]
        public IEnumerable<TrackDetail> GetLibrarySync()
        {
            using (var ctx = new LightShowContext())
            {
                return ctx.GetLibrarySync();
            }
        }

        [HttpPost]
        [Route("LibrarySync")]
        public dynamic SaveLibrarySync(LibrarySync info)
        {
            using (var ctx = new LightShowContext())
            {
                info.NewList.ForEach(t => ctx.SaveLibrarySync(t));
                info.DeleteList.ForEach(d => ctx.DeleteLibrarySync(d));
            }
            return new {Success = true};
        }
    }
}
