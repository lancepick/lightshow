﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using LightShow.Models;
using Susanoo;
using Susanoo.Processing;

namespace LightShow.DataAccess
{
    public class LightShowContext : IDisposable
    {
        private static readonly ISingleResultSetCommandProcessor<dynamic, Track> GetTracksCommand =
            CommandManager.Instance.DefineCommand("TracksGet", CommandType.StoredProcedure)
                .DefineResults<Track>()
                .Realize();

        public IEnumerable<Track> GetTracks()
        {
            return GetTracksCommand.Execute(DatabaseManager, new {});
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, Track> GetTrackCommand =
            CommandManager.Instance.DefineCommand("TrackGet", CommandType.StoredProcedure)
                .DefineResults<Track>()
                .Realize();

        public Track GetTrack(int trackId)
        {
            return GetTrackCommand.Execute(DatabaseManager, new {TrackId = trackId}).FirstOrDefault();
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, PlaylistTrack> GetNowPlayingTrackCommand =
            CommandManager.Instance.DefineCommand("NowPlayingGetTrack", CommandType.StoredProcedure)
                .DefineResults<PlaylistTrack>()
                .Realize();

        public PlaylistTrack GetNowPlayingTrack()
        {
            return GetNowPlayingTrackCommand.Execute(DatabaseManager, new {}).FirstOrDefault();
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, Track> GetPlaylistQueueCommand =
            CommandManager.Instance.DefineCommand("PlaylistGetQueue", CommandType.StoredProcedure)
                .DefineResults<Track>()
                .Realize();

        public IEnumerable<Track> GetPlaylistQueue()
        {
            return GetPlaylistQueueCommand.Execute(DatabaseManager, new {});
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, string> GetPlaylistNextTrackCommand =
            CommandManager.Instance.DefineCommand("PlaylistGetNextTrack", CommandType.StoredProcedure)
                .DefineResults<string>()
                .Realize();

        public string GetPlaylistNextTrack()
        {
            return GetPlaylistNextTrackCommand.Execute(DatabaseManager, new {}).FirstOrDefault();
        }

        private static readonly INoResultCommandProcessor<dynamic> AddTrackToPlaylistCommand =
            CommandManager.Instance.DefineCommand("PlaylistAddTrack", CommandType.StoredProcedure)
                .Realize();

        public bool AddTrackToPlaylist(int trackId)
        {
            AddTrackToPlaylistCommand.ExecuteNonQuery(DatabaseManager, new {TrackId = trackId});
            return true;
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, PlaylistTrack> GetPlaylistHistoryCommand =
            CommandManager.Instance.DefineCommand("PlaylistGetHistory", CommandType.StoredProcedure)
                .DefineResults<PlaylistTrack>()
                .Realize();

        public IEnumerable<PlaylistTrack> GetPlaylistHistory()
        {
            return GetPlaylistHistoryCommand.Execute(DatabaseManager, new {});
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, bool> GetKillSwitchCommand =
            CommandManager.Instance.DefineCommand("KillSwitchGet", CommandType.StoredProcedure)
                .DefineResults<bool>()
                .Realize();

        public bool GetKillSwitch()
        {
            return GetKillSwitchCommand.Execute(DatabaseManager, new {}).FirstOrDefault();
        }

        private static readonly INoResultCommandProcessor<dynamic> SetKillSwitchCommand =
            CommandManager.Instance.DefineCommand("KillSwitchSet", CommandType.StoredProcedure)
                .Realize();

        public void SetKillSwitch(bool isOn)
        {
            SetKillSwitchCommand.ExecuteNonQuery(DatabaseManager, new {IsOn = isOn});
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, string> GetRandomSongCommand =
            CommandManager.Instance.DefineCommand("RandomSongGet", CommandType.StoredProcedure)
                .DefineResults<string>()
                .Realize();

        public string GetRandomSong()
        {
            return GetRandomSongCommand.Execute(DatabaseManager, new {}).FirstOrDefault();
        }

        private static readonly INoResultCommandProcessor<dynamic> ClearQueueCommand =
            CommandManager.Instance.DefineCommand("QueueClear", CommandType.StoredProcedure)
                .Realize();

        public void ClearQueue()
        {
            ClearQueueCommand.ExecuteNonQuery(DatabaseManager, new {});
        }

        private static readonly ISingleResultSetCommandProcessor<dynamic, TrackDetail> GetLibrarySyncCommand =
            CommandManager.Instance.DefineCommand("LibrarySyncGetTracks", CommandType.StoredProcedure)
                .DefineResults<TrackDetail>()
                .Realize();

        public IEnumerable<TrackDetail> GetLibrarySync()
        {
            return GetLibrarySyncCommand.Execute(DatabaseManager, new { });
        }

        private static readonly INoResultCommandProcessor<dynamic> SaveLibrarySyncCommand =
            CommandManager.Instance.DefineCommand("LibrarySyncAddTrack", CommandType.StoredProcedure)
                .Realize();

        public void SaveLibrarySync(TrackDetail track)
        {
            SaveLibrarySyncCommand.ExecuteNonQuery(DatabaseManager, new
            {
                track.Album,
                track.Title,
                track.Artist,
                track.Minutes,
                track.Seconds,
                track.FilePath
            });
        }

        private static readonly INoResultCommandProcessor<dynamic> DeleteLibrarySyncCommand =
            CommandManager.Instance.DefineCommand("LibrarySyncDeleteTrack", CommandType.StoredProcedure)
                .Realize();

        public void DeleteLibrarySync(int trackId)
        {
            DeleteLibrarySyncCommand.ExecuteNonQuery(DatabaseManager, new { TrackId = trackId });
        }

        private DatabaseManager _databaseManager;

        private DatabaseManager DatabaseManager
            =>
                _databaseManager ??
                (_databaseManager = new DatabaseManager(SqlClientFactory.Instance, "LightShowConnectionString"));

        private bool _disposed;

        ~LightShowContext()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DatabaseManager.Dispose();
                }
            }

            _disposed = true;
        }
    }
}
