﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightShow.Models
{
    public class Track
    {
        public int TrackId { get; set; }
        public string Album { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public string FileName { get; set; }
    }
}
