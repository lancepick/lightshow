﻿namespace LightShow.Models
{
    public class TrackDetail : Track
    {
        public string FilePath { get; set; }
    }
}