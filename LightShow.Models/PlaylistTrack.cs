﻿using System;

namespace LightShow.Models
{
    public class PlaylistTrack : Track
    {
        public DateTime PlayedDate { get; set; }      
        public DateTime EndDate => PlayedDate.AddMinutes(Minutes).AddSeconds(Seconds + Properties.Settings.Default.TrackPadSeconds);
        public int MillisLeft => (int)Math.Floor((EndDate - DateTime.UtcNow).TotalMilliseconds);
    }
}