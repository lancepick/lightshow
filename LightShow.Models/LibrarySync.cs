﻿using System.Collections.Generic;

namespace LightShow.Models
{
    public class LibrarySync
    {
        public List<TrackDetail> NewList { get; set; }
        public List<int> DeleteList { get; set; }
    }
}